package hello;

// on line change
// Comment with very high priority !! -> Peer Review for Jacob Galvez
// assignment 9 comment

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

//mjw_223 comment for assignment 14
//nac123 comment for assignment 14
// Starting class (3-4)


public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Nick")
   public void testGreeterNick()

   {
      g.setName("Nick");
      assertEquals(g.getName(),"Nick");
      assertEquals(g.sayHello(),"Hello Nick!");
   }

   @Test
   @DisplayName("Test for Matthew")
   public void testGreeterMatthew() 

   {
      g.setName("Matthew");
      assertEquals(g.getName(),"Matthew");
      assertEquals(g.sayHello(),"Hello Matthew!");
   }

   @Test
   @DisplayName("Test for assertFalse")
   public void testAssertFalse() 

   {
      g.setName("Matthew"); 
      assertFalse((g.sayHello() == "Hello Ted!"));
      
   }

   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {

      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }

   @Test
   @DisplayName("AssertFalse Test for txst netid (jag741): ")
   public void assertFalseTest() 
   {
       boolean isNameFalse;
       g.setName("jag741");
       if (g.getName() == "jag741")
       {
           isNameFalse = false;
       }
       else
       {
           isNameFalse = true;
       }
       assertFalse(isNameFalse, "AssertFalseTest Succeeded");
   }

   @Test
   @DisplayName("(nac123) Test using assertFalse for to have minimal characters in name to 2: ")
   public void nac123_assert_false_test(){
       boolean check;
       g.setName("John");
       int count = g.getName().length();
       if(count >= 2){
          check = false; 
       }
       else{
           check = true;
       }
       
       assertFalse(check, "Minimum character test successful (nac123)");
   }

}
